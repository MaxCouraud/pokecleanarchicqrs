<?php
declare(strict_types=1);

namespace App\Game\Ui\Http\Web\Controller;

use App\Game\Domain\UseCase\ListGame\ListGame;
use App\Game\Domain\UseCase\ListGame\ListGamePresenter;
use App\Game\Domain\UseCase\ListGame\ListGameRequest;
use App\Game\Infrastructure\View\Twig\ListVideoGameView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ListGameController extends AbstractController
{
    #[Route('/game', name: 'app.game.videogame.list')]

    public function __invoke(
        Request $request,
        ListGame $listGameUseCase,
        ListGamePresenter $listGamePresenter,
        ListVideoGameView $listVideoGameView) {
        $domainRequest = new ListGameRequest();

        $listGameUseCase->execute($domainRequest, $listGamePresenter);

        return $listVideoGameView->generateView($listGamePresenter->viewModel());
    }
}
