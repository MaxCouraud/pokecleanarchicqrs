<?php
declare(strict_types=1);
namespace App\Game\Ui\Http\Web\Presenter;

use App\Game\Ui\Http\Web\ViewModel\ListGameHtmlViewModel;
use App\Game\Domain\UseCase\ListGame\ListGamePresenter;
use App\Game\Domain\UseCase\ListGame\ListGameResponse;

class ListGameHtmlPresenter implements ListGamePresenter
{
    private ListGameHtmlViewModel $viewModel;

    /**
    * @param ListGameResponse $response
    */
    public function present(ListGameResponse $response): void
    {
        $this->viewModel = new ListGameHtmlViewModel();
        $this->viewModel->statusCode = 200;
        $this->viewModel->games = $response->games;
    }

    /**
    * @return ListGameHtmlViewModel
    */
    public function viewModel(): ListGameHtmlViewModel
    {
        return $this->viewModel;
    }
}
