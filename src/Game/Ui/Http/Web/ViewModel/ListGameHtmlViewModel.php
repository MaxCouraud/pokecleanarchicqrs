<?php
declare(strict_types=1);
namespace App\Game\Ui\Http\Web\ViewModel;

class ListGameHtmlViewModel
{
    public int $statusCode;

    public iterable $games;
}
