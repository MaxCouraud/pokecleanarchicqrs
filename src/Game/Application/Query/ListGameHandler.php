<?php
declare(strict_types=1);
namespace App\Game\Application\Query;

use App\Core\Application\Query\QueryHandler;
use App\Game\Domain\Repository\VideoGameRepository;

class ListGameHandler implements QueryHandler {

    private VideoGameRepository $repository;

    public function __construct(VideoGameRepository $gameRepository)
    {
        $this->repository = $gameRepository;
    }

    public function __invoke(ListGameQuery $query)
    {
        return $this->repository->getVideoGames();
    }
}
