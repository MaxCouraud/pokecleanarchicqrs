<?php
declare(strict_types=1);
namespace App\Game\Domain\UseCase\ListGame;

use App\Core\Application\Query\QueryBus;
use App\Game\Application\Query\ListGameQuery;
use App\Game\Ui\Http\Web\Presenter\ListGameHtmlPresenter;

class ListGame
{
    private QueryBus $queryBus;


    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function execute(ListGameRequest $request, ListGameHtmlPresenter $presenter)
    {
        $reponse = new ListGameResponse();

        $reponse->games = $this->queryBus->ask(new ListGameQuery());

        $presenter->present($reponse);
    }
}
