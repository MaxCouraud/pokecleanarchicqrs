<?php
declare(strict_types=1);
namespace App\Game\Domain\UseCase\ListGame;

class ListGameResponse
{
    public iterable $games;

}
