<?php
declare(strict_types=1);
namespace App\Game\Domain\UseCase\ListGame;

use App\Game\Ui\Http\Web\ViewModel\ListGameHtmlViewModel;

interface ListGamePresenter
{
    public function present(ListGameResponse $response);

}
