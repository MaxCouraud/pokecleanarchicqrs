<?php

namespace App\Game\Infrastructure\Storage\Doctrine\Entity;

use App\Game\Domain\Model\VideoGame;

/**
 * @ORM\Entity(repositoryClass="App\Game\Infrastructure\Storage\Doctrine\Repository\DoctrineVideoGameRepository")
 */
class VideoGameEntity
{
    public int $id;

    public string $name;

    public string $description;

    /**
     * @param VideoGame $videoGame
     *
     * @return VideoGameEntity
     */
    public static function fromVideoGame(VideoGame $videoGame)
    {
        $entity = new self();
        $entity->name = $videoGame->getName();
        $entity->description = $videoGame->getDescription();
        return $entity;
    }
}
