<?php


namespace App\Game\Infrastructure\Storage\Doctrine\Repository;

use App\Game\Domain\Model\VideoGame;
use App\Game\Domain\Repository\VideoGameRepository;
use App\Game\Infrastructure\Storage\Doctrine\Entity\VideoGameEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DoctrineVideoGameRepository extends ServiceEntityRepository implements VideoGameRepository
{

    /**
     * DoctrineSupplierRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VideoGameEntity::class);
    }

    public function getVideoGames(): array
    {
        $games= $this->findAll();
        return array_map(
            function (VideoGameEntity $game) {
                return new VideoGame( $game->name, $game->description);
            },
            $games
        );
    }
}
