<?php


namespace App\Game\Infrastructure\View\Twig;

use App\Game\Ui\Http\Web\ViewModel\ListGameHtmlViewModel;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class ListVideoGameView
{
    private $twig;

    public function __construct(Environment $twig,  RouterInterface $router)
    {
        $this->twig = $twig;
    }

    public function generateView(ListGameHtmlViewModel $viewModel): Response
    {
        return new Response($this->twig->render('game/videoGame/index.html.twig', ['viewModel' => $viewModel]), $viewModel->statusCode);
    }
}
