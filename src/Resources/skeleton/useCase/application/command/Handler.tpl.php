<?= "<?php\n" ?>
<?php
$prefix = lcfirst($useCase);
?>

declare(strict_types=1);

namespace <?= $namespace; ?>;

use App\Core\Application\Command\CommandHandler;
use App\<?= $module ?>\Domain\Repository\<?= $model ?>Repository;


class <?= $class_name ?> implements CommandHandler
{

    private <?= $model ?>Repository $repository;

    public function __construct(<?= $model ?>Repository $<?= strtolower($model) ?>Repository)
    {
        $this->repository = $<?= strtolower($model) ?>Repository;
    }

    /**
    * @param <?= $useCase ?>Command $command
    */
    public function __invoke(<?= $useCase ?>Command $command): void
    {
        $this->repository->save($command-><?= strtolower($model) ?>);
    }
}
