<?= "<?php\n" ?>
<?php
$prefix = lcfirst($useCase);
?>

declare(strict_types=1);

namespace <?= $namespace; ?>;

use App\Core\Application\Query\Query;
use App\<?= $module ?>\Domain\Model\<?= $model ?>;

class <?= $class_name ?> implements Query
{
    public <?= $model ?> $<?= strtolower($model) ?>;

    /**
    * <?= $useCase ?>Command constructor.
    *
    * @param \App\<?= $module ?>\Domain\Model\<?= $model ?> $<?= strtolower($model)."\n" ?>
    */
    public function __construct(<?= $model ?> $<?= strtolower($model) ?>)
    {
        $this-><?= strtolower($model) ?> = $<?= strtolower($model) ?>;
    }
}

