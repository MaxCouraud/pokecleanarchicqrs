<?= "<?php\n" ?>
<?php
$prefix = lcfirst($useCase);
?>

declare(strict_types=1);

namespace <?= $namespace; ?>;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class <?= $class_name ?>
{

}
