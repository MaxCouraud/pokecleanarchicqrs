<?= "<?php\n" ?>

declare(strict_types=1);

namespace <?= $namespace; ?>;

use App\<?= $module; ?>\Ui\Http\Web\ViewModel\<?= $useCase; ?><?= ucfirst($format); ?>ViewModel;
use App\<?= $module; ?>\Domain\UseCase\<?= $useCase; ?>\<?= $useCase; ?>Presenter;
use App\<?= $module; ?>\Domain\UseCase\<?= $useCase; ?>\<?= $useCase; ?>Response;

class <?= $class_name ?> implements <?= $useCase; ?>Presenter
{
    private <?= $useCase; ?><?= ucfirst($format); ?>ViewModel $viewModel;

    /**
    * @param <?= $useCase; ?>Response $response
    */
    public function present(<?= $useCase; ?>Response $response): void
    {
        $this->viewModel = new <?= $useCase; ?><?= ucfirst($format); ?>ViewModel();
        // TODO hydrate your view model
    }

    /**
    * @return <?= $useCase; ?><?= ucfirst($format); ?>ViewModel
    */
    public function viewModel(): <?= $useCase; ?><?= ucfirst($format); ?>ViewModel
    {
        return $this->viewModel;
    }
}
