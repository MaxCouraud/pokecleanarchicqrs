<?= "<?php\n" ?>

<?php
$prefix = lcfirst($useCase);
?>

declare(strict_types=1);

namespace <?= $namespace; ?>;

use App\<?= $module; ?>\Domain\UseCase\<?= $useCase; ?>\<?= $useCase; ?>;
use App\<?= $module; ?>\Domain\UseCase\<?= $useCase; ?>\<?= $useCase; ?>Presenter;
use App\<?= $module; ?>\Domain\UseCase\<?= $useCase; ?>\<?= $useCase; ?>Request;
use App\<?= $module; ?>\Infrastructure\View\<?= $useCase; ?>View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class <?= $class_name ?> extends AbstractController
{
    public function __invoke(Request $request, <?= $useCase; ?> $<?= $prefix; ?>UseCase, <?= $useCase; ?>Presenter $<?= $prefix; ?>Presenter, <?= $useCase; ?>View $<?= $prefix; ?>View) {
        $domainRequest = new <?= $useCase; ?>Request();

        $<?= $prefix; ?>UseCase($domainRequest, $<?= $prefix; ?>Presenter);

        return $<?= $prefix; ?>View->generateView($<?= $prefix; ?>Presenter->viewModel());
    }
}
