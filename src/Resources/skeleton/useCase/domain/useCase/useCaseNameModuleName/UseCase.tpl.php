<?= "<?php\n" ?>

declare(strict_types=1);

namespace <?= $namespace; ?>;

use App\Core\Application\Query\QueryBus;


class <?= $class_name."\n" ?>
{
    private QueryBus $queryBus;


    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

}
