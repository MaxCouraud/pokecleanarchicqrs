<?= "<?php\n" ?>

declare(strict_types=1);

namespace <?= $namespace; ?>;

use App\<?= $module ?>\Infrastructure\Storage\Doctrine\Entity\<?= $model ?>Entity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use App\<?= $module ?>\Domain\Repository\<?= $model ?>Repository;
use Doctrine\Persistence\ManagerRegistry;
use App\<?= $module ?>\Domain\Model\<?= $model ?>;


class <?= $class_name ?> extends ServiceEntityRepository implements <?= $model ?>Repository
{
    /**
    * Doctrine<?= $model ?>Repository constructor.
    */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, <?= $model ?>Entity::class);
    }

}
