<?php

namespace App\Maker;

use App\Maker\Service\MakeUseCaseApplication;
use App\Maker\Service\MakeUseCaseDomain;
use App\Maker\Service\MakeUseCaseInfrastructure;
use App\Maker\Service\MakeUseCaseUi;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

final class MakeCleanUseCase extends AbstractMaker
{
    private $makeUseCaseApplication;
    private $makeUseCaseDomain;
    private $makeUseCaseInfrastructure;
    private $makeUseCaseUi;

    public function __construct(MakeUseCaseApplication $makeUseCaseApplication, MakeUseCaseDomain $makeUseCaseDomain, MakeUseCaseInfrastructure $makeUseCaseInfrastructure, MakeUseCaseUi $makeUseCaseUi)
    {
        $this->makeUseCaseApplication = $makeUseCaseApplication;
        $this->makeUseCaseDomain = $makeUseCaseDomain;
        $this->makeUseCaseInfrastructure = $makeUseCaseInfrastructure;
        $this->makeUseCaseUi = $makeUseCaseUi;
    }


    public static function getCommandName(): string
    {
        return 'make:clean:use-case';
    }

    public static function getCommandDescription(): string
    {
        return 'Creates a new use case classes';
    }

    public function configureCommand(Command $command, InputConfiguration $inputConf)
    {
        $command
            ->addArgument('useCase', InputArgument::OPTIONAL, sprintf('The use case name (e.g. <fg=yellow>%s</>)', Str::asClassName(Str::getRandomTerm())))
            ->addArgument('module', InputArgument::OPTIONAL, sprintf('The module name of the entity to create CRUD (e.g. <fg=yellow>%s</>)', Str::asClassName(Str::getRandomTerm())))
            ->addOption('model', 'm', InputOption::VALUE_OPTIONAL, sprintf('The class name of the domain model to create (e.g. <fg=yellow>%s</>)', Str::asClassName(Str::getRandomTerm())))
            ->addOption('cqrs', 'c', InputOption::VALUE_OPTIONAL, sprintf('You want Command or query?', Str::asClassName(Str::getRandomTerm())))
            ->addOption('format', 'f', InputOption::VALUE_OPTIONAL, sprintf('The format (e.g. <fg=yellow>%s</>)', Str::asClassName(Str::getRandomTerm())))
            ->addOption('doctrine', 'd', InputOption::VALUE_OPTIONAL, sprintf('Doctrine Entity'))
            ->setHelp("Generate use case");
    }

    public function configureDependencies(DependencyBuilder $dependencies)
    {
        // TODO: Implement configureDependencies() method
        //.
    }

    protected function init(Generator $generator)
    {
        $this->makeUseCaseApplication->setGenerator($generator);
        $this->makeUseCaseUi->setGenerator($generator);
        $this->makeUseCaseDomain->setGenerator($generator);
        $this->makeUseCaseInfrastructure->setGenerator($generator);
    }

    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator)
    {
        $this->init($generator);
        $useCaseName = trim($input->getArgument('useCase'));
        $module = trim($input->getArgument('module'));
        $model = trim($input->getOption('model'));

        if(!$model){
            $input->setOption(
                'model',
                $io->ask(
                    'Add model ? leave blank if no model interaction (e.g. <fg=yellow>Product</>)',
                    null
                )
            );
        }

        $model = trim($input->getOption('model'));


        //generate CQRS

        $command = trim($input->getOption('cqrs'));
        if (!$command) {
            $input->setOption(
                'cqrs',
                $io->choice(
                    '(e.g. <fg=yellow>Product</>)',
                    ['command', 'query'],
                    null
                )
            );
            if ($input->getOption("cqrs") == "command") {
                $this->makeUseCaseApplication->generateApplicationCommand($useCaseName, $module, $model);
                $this->makeUseCaseApplication->generateApplicationCommandHandler($useCaseName, $module, $model);
            }
            if ($input->getOption("cqrs") == "query") {
                $this->makeUseCaseApplication->generateApplicationQuery($useCaseName, $module, $model);
                $this->makeUseCaseApplication->generateApplicationQueryHandler($useCaseName, $module);
            }
        }

        //generate doctine entity if it does not exist

        if(!file_exists('src/' . $module . '/Infrastructure/Persistence/Doctrine/Entity/' . $model . 'Entity.php')
            || !file_exists('src/' . $module . '/Infrastructure/Persistence/Doctrine/Repository/Doctrine' . $model . 'Repository.php')
            || !file_exists('src/' . $module . '/Infrastructure/Persistence/Doctrine/Mapping/' . $model . 'Entity.orm.xml')){
            $input->setOption(
                'doctrine',
                $io->choice(
                    'Do you want to generate a <fg=yellow>Doctrine Entity</>?',
                    ['yes', 'no'],
                    null
                )
            );
            if(!empty($module) && !empty($model))
            {
                if ($input->getOption("doctrine") == "yes") {
                    $this->makeUseCaseInfrastructure->generateInfrastructureDoctrineEntity($model, $module);
                    $this->makeUseCaseInfrastructure->generateInfrastructureDoctrineXmlMapping($model, $module);
                    $this->makeUseCaseInfrastructure->generateInfrastructureDoctrineRepository($model, $module);
                }
            }
        }


        $format = trim($input->getOption('format'));
        if (!$format) {
            $input->setOption(
                'format',
                $io->choice(
                    'What kind of <fg=yellow>view</> do you want',
                    ['twig', 'json', 'cli'],
                    null
                )
            );
            if ($input->getOption("format") == "cli") {
                $this->makeUseCaseUi->generateUiCli($module, $useCaseName);
            }
            if ($input->getOption("format") == "json") {
                $this->makeUseCaseUi->generateUiHttpApi($module, $useCaseName);
            }
            if ($input->getOption("format") == "twig") {
                $this->makeUseCaseUi->generateUiHttpWeb($useCaseName, $module);
            }
        }



        //Generate Domain Class

        $this->makeUseCaseDomain->generateDomainUseCaseClass($useCaseName, $module, $model);
        $this->makeUseCaseDomain->generateDomainRequestClass($useCaseName, $module);
        $this->makeUseCaseDomain->generateDomainResponseClass($useCaseName, $module);
        $this->makeUseCaseDomain->generateDomainPresenterInterface($useCaseName, $module);

        if ($model) {
            if(!file_exists('src/' . $module . '/Domain/Model/' . $model . '.php')){
                $this->makeUseCaseDomain->generateDomainModelClass($module, $model);
            }

            if(!file_exists('src/' . $module . '/Domain/Repository/' . $model . 'Repository.php')){
                $this->makeUseCaseDomain->generateDomainRepositoryInterface($module, $model);
            }

        }



        $generator->writeChanges();

        $this->writeSuccessMessage($io);
        $io->text([
            'Next: open your new command class and customize it!',
            'Find the documentation at <fg=yellow>https://symfony.com/doc/current/console.html</>',
        ]);
    }
}
