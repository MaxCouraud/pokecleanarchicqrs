<?php


namespace App\Maker\Service;


use App\Maker\AbstractGenerator;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\Str;

class MakeUseCaseUi extends AbstractGenerator
{

    protected $generator;

    private function generateBaseUiNamespace(string $module)
    {
        return $module . '\\Ui\\';
    }

    public function generateUiCli(string $module, string $useCase)
    {
        $commandClassNameDetails = $this->generator->createClassNameDetails(
            $useCase,
            $this->generateBaseUiNamespace($module) . 'Cli\\Command\\',
            'Command',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCase, Str::asClassName($useCase, 'Repository'))
        );

        if (!class_exists($commandClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $commandClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/ui/cli/Cli.tpl.php',
                [
                    'class_name' => $commandClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($commandClassNameDetails->getFullName()),
                    'module' => $module,
                    'useCase' => $useCase,
                ]
            );
        }
        $presenterClassNameDetails = $this->generator->createClassNameDetails(
            $useCase,
            $this->generateBaseUiNamespace($module) . 'Cli\\Presenter\\',
            'Presenter',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCase, Str::asClassName($useCase, 'Repository'))
        );

        if (!class_exists($presenterClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $presenterClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/ui/cli/Presenter.tpl.php',
                [
                    'class_name' => $presenterClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($presenterClassNameDetails->getFullName()),
                    'module' => $module,
                    'useCase' => $useCase,
                ]
            );
        }
        $viewModelClassNameDetails = $this->generator->createClassNameDetails(
            $useCase,
            $this->generateBaseUiNamespace($module) . 'Cli\\viewModel\\',
            'viewModel',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCase, Str::asClassName($useCase, 'Repository'))
        );

        if (!class_exists($viewModelClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $viewModelClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/ui/cli/ViewModel.tpl.php',
                [
                    'class_name' => $viewModelClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($viewModelClassNameDetails->getFullName()),
                    'module' => $module,
                    'useCase' => $useCase,
                ]
            );
        }
    }

    public function generateUiHttpApi(string $module, string $useCase)
    {
        $controllerClassNameDetails = $this->generator->createClassNameDetails(
            $useCase,
            $this->generateBaseUiNamespace($module) . 'Http\\Api\\Controller\\',
            'Controller',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCase, Str::asClassName($useCase, 'Repository'))
        );

        if (!class_exists($controllerClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $controllerClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/ui/http/api/controller/UseCaseController.tpl.php',
                [
                    'class_name' => $controllerClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($controllerClassNameDetails->getFullName()),
                    'module' => $module,
                    'useCase' => $useCase,
                ]
            );
        }
        $presenterClassNameDetails = $this->generator->createClassNameDetails(
            $useCase,
            $this->generateBaseUiNamespace($module) . 'Http\\Api\\Presenter\\',
            'Json'.'Presenter',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCase, Str::asClassName($useCase, 'Repository'))
        );

        if (!class_exists($presenterClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $presenterClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/ui/http/api/presenter/UseCaseJsonPresenter.stl.php',
                [
                    'class_name' => $presenterClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($presenterClassNameDetails->getFullName()),
                    'module' => $module,
                    'useCase' => $useCase,
                ]
            );
        }
        $viewModelClassNameDetails = $this->generator->createClassNameDetails(
            $useCase,
            $this->generateBaseUiNamespace($module) . 'Http\\Api\\viewModel\\',
            'Json'.'ViewModel',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCase, Str::asClassName($useCase, 'Repository'))
        );

        if (!class_exists($viewModelClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $viewModelClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/ui/http/api/viewModel/UseCaseJsonViewModel.stl.php',
                [
                    'class_name' => $viewModelClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($viewModelClassNameDetails->getFullName()),
                    'module' => $module,
                    'useCase' => $useCase,
                ]
            );
        }
    }

    public function generateUiHttpWebForm(string $useCase, string $module)
    {
        $formClassNameDetails = $this->generator->createClassNameDetails(
            $useCase,
            $this->generateBaseUiNamespace($module) . 'Http\\Web\\Form\\',
            'Form',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCase, Str::asClassName($useCase, 'Repository'))
        );

        if (!class_exists($formClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $formClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/ui/http/web/form/UseCaseForm.stl.php',
                [
                    'class_name' => $formClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($formClassNameDetails->getFullName()),
                    'module' => $module,
                    'useCase' => $useCase,
                ]
            );
        }
    }

    public function generateUiHttpWeb(string $useCase, string $module)
    {
        $controllerClassNameDetails = $this->generator->createClassNameDetails(
            $useCase,
            $this->generateBaseUiNamespace($module) . 'Http\\Web\\Controller\\',
            'Controller',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCase, Str::asClassName($useCase, 'Repository'))
        );

        if (!class_exists($controllerClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $controllerClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/ui/http/web/controller/UseCaseController.stl.php',
                [
                    'class_name' => $controllerClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($controllerClassNameDetails->getFullName()),
                    'useCase' => $useCase,
                    'module' => $module
                ]
            );
        }

        $presenterClassNameDetails = $this->generator->createClassNameDetails(
            $useCase,
            $this->generateBaseUiNamespace($module) . 'Http\\Web\\Presenter\\',
            'Html'.'Presenter',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCase, Str::asClassName($useCase, 'Repository'))
        );

        if (!class_exists($presenterClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $presenterClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/ui/http/web/presenter/UseCaseHtmlPresenter.stl.php',
                [
                    'class_name' => $presenterClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($presenterClassNameDetails->getFullName()),
                    'useCase' => $useCase,
                    'module' => $module,
                    'format' => 'Html'
                ]
            );
        }
        $viewModelClassNameDetails = $this->generator->createClassNameDetails(
            $useCase,
            $this->generateBaseUiNamespace($module) . 'Http\\Web\\ViewModel\\',
            'Html'.'ViewModel',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCase, Str::asClassName($useCase, 'Repository'))
        );

        if (!class_exists($viewModelClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $viewModelClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/ui/http/web/viewModel/UseCaseHtmlViewModel.stl.php',
                [
                    'class_name' => $viewModelClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($viewModelClassNameDetails->getFullName()),
                    'useCase' => $useCase,
                    'module' => $module,
                ]
            );
        }
    }
}
