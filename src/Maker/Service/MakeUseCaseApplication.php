<?php


namespace App\Maker\Service;


use App\Maker\AbstractGenerator;
use Symfony\Bundle\MakerBundle\Str;

class
MakeUseCaseApplication extends AbstractGenerator
{

    private function generateBaseApplicationNamespace(string $module)
    {
        return $module . '\\Application\\';
    }

    public function generateApplicationCommand(string $useCaseName, string $module, string $model)
    {
        $commandClassNameDetails = $this->generator->createClassNameDetails(
            $useCaseName,
            $this->generateBaseApplicationNamespace($module) . 'Command\\',
             'Command',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCaseName, Str::asClassName($useCaseName, 'Command'))
        );

        $this->generator->generateClass(
            $commandClassNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/useCase/application/command/Command.tpl.php',
            [
                'class_name' => $commandClassNameDetails->getShortName(),
                'namespace' => Str::getNamespace($commandClassNameDetails->getFullName()),
                'useCase' => $useCaseName,
                'module' => $module,
                'model' => $model
            ]
        );
    }

    public function generateApplicationCommandHandler(string $useCaseName, string $module, string $model)
    {
        $commandHandlerClassNameDetails = $this->generator->createClassNameDetails(
            $useCaseName,
            $this->generateBaseApplicationNamespace($module) . 'Command\\',
            'Handler',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCaseName, Str::asClassName($useCaseName, 'Command'))
        );

        $this->generator->generateClass(
            $commandHandlerClassNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/useCase/application/command/Handler.tpl.php',
            [
                'class_name' => $commandHandlerClassNameDetails->getShortName(),
                'namespace' => Str::getNamespace($commandHandlerClassNameDetails->getFullName()),
                'useCase' => $useCaseName,
                'module' => $module,
                'model' => $model,
            ]
        );
    }

    public function generateApplicationQuery(string $useCaseName, string $module, string $model)
    {
        $queryClassNameDetails = $this->generator->createClassNameDetails(
            $useCaseName,
            $this->generateBaseApplicationNamespace($module) . 'Query\\',
             'Query',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCaseName, Str::asClassName($useCaseName, 'Command'))
        );

        $this->generator->generateClass(
            $queryClassNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/useCase/application/query/Query.tpl.php',
            [
                'class_name' => $queryClassNameDetails->getShortName(),
                'namespace' => Str::getNamespace($queryClassNameDetails->getFullName()),
                'useCase' => $useCaseName,
                'module' => $module,
                'model' => $model
            ]
        );
    }

    public function generateApplicationQueryHandler(string $useCaseName, string $module)
    {
        $queryHandlerClassNameDetails = $this->generator->createClassNameDetails(
            $useCaseName,
            $this->generateBaseApplicationNamespace($module) . 'Query\\',
            'Handler',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCaseName, Str::asClassName($useCaseName, 'Command'))
        );

        $this->generator->generateClass(
            $queryHandlerClassNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/useCase/application/query/Handler.tpl.php',
            [
                'class_name' => $queryHandlerClassNameDetails->getShortName(),
                'namespace' => Str::getNamespace($queryHandlerClassNameDetails->getFullName()),
                'useCase' => $useCaseName,
                'module' => $module,
            ]
        );
    }
}
