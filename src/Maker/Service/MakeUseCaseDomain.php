<?php


namespace App\Maker\Service;


use App\Maker\AbstractGenerator;
use Symfony\Bundle\MakerBundle\Str;

class MakeUseCaseDomain extends AbstractGenerator
{
    private function generateBaseDomainNamespace(string $module)
    {
        return $module . '\\Domain\\';
    }

    public function generateDomainModelClass(string $module, string $model = null)
    {
        $modelClassNameDetails = $this->generator->createClassNameDetails(
            $model,
            $this->generateBaseDomainNamespace($module)  . 'Model\\',
            '',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $model, Str::asClassName($model, ''))
        );

        if (!class_exists($modelClassNameDetails->getFullName())) {
            $this->generator->generateClass(
                $modelClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/domain/model/Model.tpl.php',
                [
                    'class_name' => $modelClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($modelClassNameDetails->getFullName()),
                    'module' => $module,
                    'model' => $model,
                ]
            );
        }
    }


    public function generateDomainRepositoryInterface(string $module, string $model)
    {
        $repositoryClassNameDetails = $this->generator->createClassNameDetails(
            $model,
            $this->generateBaseDomainNamespace($module) . 'Repository\\',
            'Repository',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $model, Str::asClassName($model, 'Repository'))
        );

        if (!file_exists('src/' . $module . '/Domain/Repository/' . $model . 'Repository.php')) {

            $this->generator->generateClass(
                $repositoryClassNameDetails->getFullName(),
                __DIR__ . '/../../Resources/skeleton/useCase/domain/repository/Repository.tpl.php',
                [
                    'class_name' => $repositoryClassNameDetails->getShortName(),
                    'namespace' => Str::getNamespace($repositoryClassNameDetails->getFullName()),
                    'module' => $module,
                    'model' => $model,
                ]
            );
        }
    }

    public function generateDomainUseCaseClass(string $useCaseName, string $module, string $model)
    {
        $useCaseClassNameDetails = $this->generator->createClassNameDetails(
            $useCaseName,
            $this->generateBaseDomainNamespace($module) . 'UseCase\\' . $useCaseName . '\\',
             '',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCaseName, Str::asClassName($useCaseName, 'Command'))
        );

        $this->generator->generateClass(
            $useCaseClassNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/useCase/domain/useCase/useCaseNameModuleName/UseCase.tpl.php',
            [
                'class_name' => $useCaseClassNameDetails->getShortName(),
                'namespace' => Str::getNamespace($useCaseClassNameDetails->getFullName()),
                'module' => $module,
                'useCaseName' => $useCaseName,
                'model' => $model
            ]
        );
    }

    public function generateDomainRequestClass(string $useCaseName, string $module)
    {
        $requestClassNameDetails = $this->generator->createClassNameDetails(
            $useCaseName,
            $this->generateBaseDomainNamespace($module) . 'UseCase\\' . $useCaseName  . '\\',
             'Request',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCaseName, Str::asClassName($useCaseName, 'Command'))
        );

        $this->generator->generateClass(
            $requestClassNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/useCase/domain/useCase/useCaseNameModuleName/Request.tpl.php',
            [
                'class_name' => $requestClassNameDetails->getShortName(),
                'namespace' => Str::getNamespace($requestClassNameDetails->getFullName()),
            ]
        );
    }

    public function generateDomainResponseClass(string $useCaseName, string $module)
    {
        $responseClassNameDetails = $this->generator->createClassNameDetails(
            $useCaseName,
            $this->generateBaseDomainNamespace($module) . 'UseCase\\' . $useCaseName . '\\',
            'Response',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCaseName, Str::asClassName($useCaseName, 'Command'))
        );

        $this->generator->generateClass(
            $responseClassNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/useCase/domain/useCase/useCaseNameModuleName/Response.tpl.php',
            [
                'class_name' => $responseClassNameDetails->getShortName(),
                'namespace' => Str::getNamespace($responseClassNameDetails->getFullName()),
            ]
        );
    }

    public function generateDomainPresenterInterface(string $useCaseName, string $module)
    {
        $prensenterInterfaceNameDetails = $this->generator->createClassNameDetails(
            $useCaseName,
            $this->generateBaseDomainNamespace($module) . 'UseCase\\' . $useCaseName . '\\',
             'Presenter',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCaseName, Str::asClassName($useCaseName, 'Command'))
        );

        $this->generator->generateClass(
            $prensenterInterfaceNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/useCase/domain/useCase/useCaseNameModuleName/Presenter.tpl.php',
            [
                'class_name' => $prensenterInterfaceNameDetails->getShortName(),
                'namespace' => Str::getNamespace($prensenterInterfaceNameDetails->getFullName()),
                'useCase' => $useCaseName,
            ]
        );
    }
}
