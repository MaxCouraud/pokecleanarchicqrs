<?php


namespace App\Maker\Service;


use App\Maker\AbstractGenerator;
use Symfony\Bundle\MakerBundle\Str;

class MakeUseCaseInfrastructure extends AbstractGenerator
{
    private function generateBaseInfrastructureNamespace(string $module)
    {
        return $module . '\\Infrastructure\\';
    }

    public function generateInfrastructureDoctrineEntity(string $model, string $module)
    {

        $entityNameDetails = $this->generator->createClassNameDetails(
            $model,
            $this->generateBaseInfrastructureNamespace($module) . 'Persistence\\' . 'Doctrine\\' . 'Entity\\',
            'Entity',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $module, Str::asClassName($module, 'Command'))
        );
        $this->generator->generateClass(
            $entityNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/useCase/infrastructure/persistence/doctrine/entity/modelEntity.tpl.php',
            [
                'class_name' => $entityNameDetails->getShortName(),
                'namespace' => Str::getNamespace($entityNameDetails->getFullName()),
                'module' => $module,
                'model' => $model,
            ]
        );
    }
    public function generateInfrastructureDoctrineRepository(string $model, string $module)
    {
        $repositoryNameDetails = $this->generator->createClassNameDetails(
            'Doctrine' . $model,
            $this->generateBaseInfrastructureNamespace($module) . 'Persistence\\' . 'Doctrine\\' . 'Repository\\',
            'Repository',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $module, Str::asClassName($module, 'Command'))
        );

        $this->generator->generateClass(
            $repositoryNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/useCase/infrastructure/persistence/doctrine/repository/doctrineModelRepository.tpl.php',
            [
                'class_name' => $repositoryNameDetails->getShortName(),
                'namespace' => Str::getNamespace($repositoryNameDetails->getFullName()),
                'module' => $module,
                'model' => $model,
            ]
        );
    }
    public function generateInfrastructureDoctrineXmlMapping(string $model, string $module)
    {
        $this->generator->generateFile(
            __DIR__ . '/../../' . $module .'/Infrastructure/Persistence/Doctrine/Mapping/'. $model . 'Entity.orm.xml',
            __DIR__ . '/../../Resources/skeleton/useCase/infrastructure/persistence/doctrine/mapping/modelEntity.orm.tpl.xml',
            [
                'entityPath' => 'App/' . $module .'/Infrastructure/Persistence/Doctrine/Entity/' . $model . 'Entity',
                'table' => strtolower($model),
                'repositoryClass' => 'App\\' . $module .'\\Infrastructure\\Persistence\\Doctrine\\Repository\\Doctrine' . $model. 'Repository',
                'module' => $module,
                'model' => $model,
            ]
        );
    }
    public function generateInfrastructureViewCli(string $useCaseName, string $module)
    {
        $viewCliNameDetails = $this->generator->createClassNameDetails(
             $useCaseName,
            $this->generateBaseInfrastructureNamespace($module) . 'Persistence\\' . $useCaseName . '\\',
            'Repository',
            sprintf('The "%s" command name is not valid because it would be implemented by "%s" class, which is not valid as a PHP class name (it must start with a letter or underscore, followed by any number of letters, numbers, or underscores).', $useCaseName, Str::asClassName($useCaseName, 'Command'))
        );
        $this->generator->generateClass(
            $viewCliNameDetails->getFullName(),
            __DIR__ . '/../../Resources/skeleton/infrastructure/persistence/doctrine/repository/doctrineModelRepository.tpl.php',
            [
                'class_name' => $viewCliNameDetails->getShortName(),
                'namespace' => Str::getNamespace($viewCliNameDetails->getFullName()),
                'module' => $module,
                'useCaseName' => $useCaseName,
            ]
        );
    }

    public function generateInfrastructureViewTwig(string $useCaseName, string $module)
    {
        //TODO
    }

    public function generateInfrastructureViewJson(string $useCaseName, string $module)
    {
        //TODO
    }
}
