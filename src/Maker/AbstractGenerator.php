<?php


namespace App\Maker;


use Symfony\Bundle\MakerBundle\Generator;

abstract class AbstractGenerator
{

    protected $generator;

    /**
     * @return Generator
     */
    public function getGenerator(): Generator
    {
        return $this->generator;
    }

    /**
     * @param Generator $generator
     */
    public function setGenerator(Generator $generator): void
    {
        $this->generator = $generator;
    }

}
