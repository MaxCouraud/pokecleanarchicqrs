<?php

namespace App\Supply\Ui\Http\Api\Controller;

use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplier;
use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplierPresenter;
use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplierRequest;
use App\Supply\Infrastructure\View\Json\CreateSupplierView;
use App\Supply\Ui\Http\Web\Form\SupplierFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CreateSupplierController extends AbstractController
{

    #[Route('/api/supplier', name: 'app.api.supply.supplier.create', methods: ['POST'])]
    public function __invoke(
      Request $request,
      CreateSupplier $createSupplierUseCase,
      CreateSupplierPresenter $createSupplierPresenter,
      CreateSupplierView $createSupplierView
    ) {
        $domainRequest = new CreateSupplierRequest();

        $form = $this->createForm(SupplierFormType::class, $domainRequest, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $createSupplierUseCase($domainRequest, $createSupplierPresenter);
            return $createSupplierView->generateView($createSupplierPresenter->viewModel());
        }

        return $createSupplierView->generateFormView($form);
    }

}
