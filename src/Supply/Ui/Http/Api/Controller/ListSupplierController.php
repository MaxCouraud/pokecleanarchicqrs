<?php

namespace App\Supply\Ui\Http\Api\Controller;

use App\Supply\Domain\UseCase\ListSupplier\ListSupplier;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierPresenter;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierRequest;
use App\Supply\Infrastructure\View\Json\ListSupplierView;
use App\Supply\Ui\Http\Api\Presenter\ListSupplierJsonPresenter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class   ListSupplierController extends AbstractController
{

    /**
     * @param Request $request
     * @param ListSupplier $listSupplierUseCase
     * @param ListSupplierJsonPresenter $listSupplierPresenter
     * @param ListSupplierView $listSupplierView
     *
     * @return Response
     */
    #[Route('/api/supplier', name: 'app.api.supply.supplier.list', methods: ['GET'])]
    public function __invoke(
      Request $request,
      ListSupplier $listSupplierUseCase,
      ListSupplierJsonPresenter $listSupplierPresenter,
      ListSupplierView $listSupplierView
    ) {
        $domainRequest = new ListSupplierRequest();

        $listSupplierUseCase->execute($domainRequest, $listSupplierPresenter);

        return $listSupplierView->generateView($listSupplierPresenter->viewModel());
    }

}
