<?php

namespace App\Supply\Ui\Http\Api\ViewModel;

use App\Supply\Domain\Model\Supplier;

class CreateSupplierJsonViewModel
{

    public int $statusCode;

    public ?Supplier $supplier;

}