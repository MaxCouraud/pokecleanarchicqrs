<?php

namespace App\Supply\Ui\Http\Api\ViewModel;

class ListSupplierJsonViewModel
{

    public int $statusCode;

    public iterable $suppliers;

}