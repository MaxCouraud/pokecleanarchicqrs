<?php

namespace App\Supply\Ui\Http\Api\Presenter;

use App\Supply\Domain\UseCase\ListSupplier\ListSupplierPresenter;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierResponse;
use App\Supply\Ui\Http\Api\ViewModel\ListSupplierJsonViewModel;

class ListSupplierJsonPresenter implements ListSupplierPresenter
{
    private ListSupplierJsonViewModel $viewModel;
    /**
    * @param ListSupplierResponse $response
    */
    public function present(ListSupplierResponse $response): void
    {
        $this->viewModel = new ListSupplierJsonViewModel();
        $this->viewModel->suppliers = $response->suppliers;
        // TODO hydrate your view model
    }

    /**
    * @return ListSupplierJsonViewModel
    */
    public function viewModel(): ListSupplierJsonViewModel
    {
        return $this->viewModel;
    }
}