<?php

namespace App\Supply\Ui\Http\Api\Presenter;

use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplierPresenter;
use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplierResponse;
use App\Supply\Ui\Http\Api\ViewModel\CreateSupplierJsonViewModel;
use App\Supply\Ui\Http\Web\ViewModel\CreateSupplierHtmlViewModel;

class CreateSupplierJsonPresenter implements CreateSupplierPresenter
{
    private CreateSupplierJsonViewModel $viewModel;
    /**
    * @param CreateSupplierResponse $response
    */
    public function present(CreateSupplierResponse $response): void
    {
        $this->viewModel = new CreateSupplierJsonViewModel();
        $this->viewModel->statusCode = 201;
        $this->viewModel->supplier = $response->supplier;
        // TODO hydrate your view model
    }

    /**
    * @return CreateSupplierJsonViewModel
    */
    public function viewModel(): CreateSupplierJsonViewModel
    {
        return $this->viewModel;
    }
}