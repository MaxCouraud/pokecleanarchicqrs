<?php

namespace App\Supply\Ui\Http\Web\ViewModel;

class ListSupplierHtmlViewModel
{
    public int $statusCode;

    public iterable $suppliers;
}