<?php

namespace App\Supply\Ui\Http\Web\ViewModel;

use App\Supply\Domain\Model\Supplier;
use Symfony\Component\Form\FormInterface;

class CreateSupplierHtmlViewModel
{
    public int $statusCode;

    public string $redirect;

    public iterable $errors;

    public FormInterface $form;

}