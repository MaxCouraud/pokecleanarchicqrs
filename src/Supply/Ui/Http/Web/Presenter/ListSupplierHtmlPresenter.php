<?php

namespace App\Supply\Ui\Http\Web\Presenter;

use App\Supply\Domain\UseCase\ListSupplier\ListSupplierPresenter;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierResponse;
use App\Supply\Ui\Http\Web\ViewModel\ListSupplierHtmlViewModel;
use App\Supply\Ui\Http\Web\ViewModel\ListSupplierJsonViewModel;

class ListSupplierHtmlPresenter implements ListSupplierPresenter
{
    private ListSupplierHtmlViewModel $viewModel;
    /**
    * @param ListSupplierResponse $response
    */
    public function present(ListSupplierResponse $response): void
    {
        $this->viewModel = new ListSupplierHtmlViewModel();
        $this->viewModel->statusCode = 200;
        $this->viewModel->suppliers = $response->suppliers;
    }

    /**
    * @return ListSupplierHtmlViewModel
    */
    public function viewModel(): ListSupplierHtmlViewModel
    {
        return $this->viewModel;
    }
}