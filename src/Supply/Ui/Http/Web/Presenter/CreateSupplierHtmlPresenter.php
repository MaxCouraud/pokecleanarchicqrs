<?php

namespace App\Supply\Ui\Http\Web\Presenter;

use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplierPresenter;
use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplierResponse;
use App\Supply\Ui\Http\Web\ViewModel\CreateSupplierHtmlViewModel;

class CreateSupplierHtmlPresenter implements CreateSupplierPresenter
{

    private CreateSupplierHtmlViewModel $viewModel;

    /**
     * @param CreateSupplierResponse $response
     */
    public function present(CreateSupplierResponse $response): void
    {
        $this->viewModel = new CreateSupplierHtmlViewModel();
        if (!empty($response->errors)) {
            $this->viewModel->statusCode = 500;
            $this->viewModel->errors = $response->errors;
        } else {
            $this->viewModel->statusCode = 302;
            $this->viewModel->redirect = "/supplier";
            $this->viewModel->errors = [];
        }
        // TODO hydrate your view model
    }

    /**
     * @return CreateSupplierHtmlViewModel
     */
    public function viewModel(): CreateSupplierHtmlViewModel
    {
        return $this->viewModel;
    }

}