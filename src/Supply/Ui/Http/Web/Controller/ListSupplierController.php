<?php

namespace App\Supply\Ui\Http\Web\Controller;

use App\Supply\Domain\UseCase\ListSupplier\ListSupplier;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierRequest;
use App\Supply\Infrastructure\View\Twig\ListSupplierView;
use App\Supply\Ui\Http\Web\Presenter\ListSupplierHtmlPresenter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListSupplierController extends AbstractController
{

    /**
     * @param Request $request
     * @param ListSupplier $listSupplierUseCase
     * @param ListSupplierHtmlPresenter $listSupplierPresenter
     * @param ListSupplierView $listSupplierView
     *
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    #[Route('/supplier', name: 'app.supply.supplier.list')]
    public function __invoke(
      Request $request,
      ListSupplier $listSupplierUseCase,
      ListSupplierHtmlPresenter $listSupplierPresenter,
      ListSupplierView $listSupplierView
    ): Response {
        $domainRequest = new ListSupplierRequest();

        $listSupplierUseCase->execute($domainRequest, $listSupplierPresenter);

        return $listSupplierView->generateView($listSupplierPresenter->viewModel());
    }

}