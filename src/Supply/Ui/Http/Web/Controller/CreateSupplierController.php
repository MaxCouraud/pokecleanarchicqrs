<?php

namespace App\Supply\Ui\Http\Web\Controller;

use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplier;
use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplierPresenter;
use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplierRequest;
use App\Supply\Infrastructure\View\Twig\CreateSupplierView;
use App\Supply\Ui\Http\Web\Form\SupplierFormType;
use App\Supply\Ui\Http\Web\Presenter\CreateSupplierHtmlPresenter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CreateSupplierController extends AbstractController
{

    #[Route('/supplier/add', name: 'app.supply.supplier.create')]
    public function __invoke(
      Request $request,
      CreateSupplier $createSupplierUseCase,
      CreateSupplierHtmlPresenter $createSupplierPresenter,
      CreateSupplierView $createSupplierView
    ) {
        $domainRequest = new CreateSupplierRequest();

        $form = $this->createForm(SupplierFormType::class, $domainRequest, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $createSupplierUseCase($domainRequest, $createSupplierPresenter);
            $createSupplierPresenter->viewModel()->form = $form;
            return $createSupplierView->generateView($createSupplierPresenter->viewModel());
        }

        return $createSupplierView->generateFormView($form);
    }

}
