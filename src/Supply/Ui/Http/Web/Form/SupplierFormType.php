<?php

namespace App\Supply\Ui\Http\Web\Form;

use App\Supply\Domain\UseCase\CreateSupplier\CreateSupplierRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SupplierFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
          ->add('url');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
          'data_class' => CreateSupplierRequest::class
                               ]);
    }

    public function getBlockPrefix(): string
    {
        return 'supply_supplier';
    }

}