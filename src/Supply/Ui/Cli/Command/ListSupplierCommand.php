<?php


namespace App\Supply\Ui\Cli\Command;


use App\Supply\Domain\UseCase\ListSupplier\ListSupplier;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierPresenter;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierRequest;
use App\Supply\Infrastructure\View\Csv\ListSupplierView;
use App\Supply\Ui\Cli\Presenter\ListSupplierCliPresenter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListSupplierCommand extends Command
{

    /**
     * @var \App\Supply\Domain\UseCase\ListSupplier\ListSupplier
     */
    private ListSupplier $listSupplierUseCase;

    /**
     * @var ListSupplierCliPresenter
     */
    private ListSupplierCliPresenter $listSupplierPresenter;

    /**
     * @var \App\Supply\Infrastructure\View\Cli\ListSupplierView
     */
    private ListSupplierView $view;

    /**
     * ListSupplierCommand constructor.
     *
     * @param \App\Supply\Domain\UseCase\ListSupplier\ListSupplier $listSupplierUseCase
     * @param \App\Supply\Ui\Cli\Presenter\ListSupplierCliPresenter $listSupplierPresenter
     * @param \App\Supply\Infrastructure\View\Cli\ListSupplierView $view
     */
    public function __construct(
      ListSupplier $listSupplierUseCase,
      ListSupplierCliPresenter $listSupplierPresenter,
      ListSupplierView $view
    ) {
        parent::__construct('app:supply:list-supplier');
        $this->listSupplierUseCase = $listSupplierUseCase;
        $this->listSupplierPresenter = $listSupplierPresenter;
        $this->view = $view;
    }

    public function configure()
    {
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $domainRequest = new ListSupplierRequest();

        $this->listSupplierUseCase->execute($domainRequest, $this->listSupplierPresenter);

        $output->write($this->view->generateView($this->listSupplierPresenter->viewModel()));

        return 0;
    }

}