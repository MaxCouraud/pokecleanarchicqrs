<?php

namespace App\Supply\Ui\Cli\ViewModel;

class ListSupplierCliViewModel
{
    public iterable $suppliers;
}