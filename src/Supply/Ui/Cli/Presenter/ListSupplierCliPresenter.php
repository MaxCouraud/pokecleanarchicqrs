<?php


namespace App\Supply\Ui\Cli\Presenter;


use App\Supply\Domain\UseCase\ListSupplier\ListSupplierPresenter;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierResponse;
use App\Supply\Ui\Cli\ViewModel\ListSupplierCliViewModel;

class ListSupplierCliPresenter implements ListSupplierPresenter
{

    private ListSupplierCliViewModel $viewModel;
    /**
     * @param ListSupplierResponse $response
     */
    public function present(ListSupplierResponse $response): void
    {
        $this->viewModel = new ListSupplierCliViewModel();
        $this->viewModel->suppliers = $response->suppliers;
        // TODO hydrate your view model
    }

    /**
     * @return ListSupplierCliViewModel
     */
    public function viewModel(): ListSupplierCliViewModel
    {
        return $this->viewModel;
    }


}