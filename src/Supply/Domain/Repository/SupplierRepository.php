<?php

namespace App\Supply\Domain\Repository;

use App\Supply\Domain\Model\Supplier;

interface SupplierRepository
{
    public function getSuppliers(): iterable;

    public function save(Supplier $supplier): void;
}