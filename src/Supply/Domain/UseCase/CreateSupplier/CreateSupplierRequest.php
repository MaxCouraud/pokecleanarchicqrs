<?php

namespace App\Supply\Domain\UseCase\CreateSupplier;

class CreateSupplierRequest
{

    public string $name;

    public string $url;

}