<?php

namespace App\Supply\Domain\UseCase\CreateSupplier;

use App\Supply\Domain\Model\Supplier;

class CreateSupplierResponse
{

    public ?Supplier $supplier = null;

    public iterable $errors;

}