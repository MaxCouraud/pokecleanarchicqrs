<?php

namespace App\Supply\Domain\UseCase\CreateSupplier;

interface CreateSupplierPresenter
{
    public function present(CreateSupplierResponse $response);
}