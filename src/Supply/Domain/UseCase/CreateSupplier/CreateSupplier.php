<?php

namespace App\Supply\Domain\UseCase\CreateSupplier;


use App\Core\Application\Command\CommandBus;
use App\Supply\Application\Command\CreateSupplierCommand;
use App\Supply\Domain\Model\Supplier;
use App\Supply\Domain\Repository\SupplierRepository;

class CreateSupplier
{

    private SupplierRepository $supplierRepository;

    private CommandBus $commandBus;

    public function __construct(SupplierRepository $supplierRepository, CommandBus $commandBus)
    {
        $this->supplierRepository = $supplierRepository;
        $this->commandBus = $commandBus;
    }

    public function __invoke(CreateSupplierRequest $request, CreateSupplierPresenter $presenter): void
    {
        $response = new CreateSupplierResponse();

        $supplier = new Supplier($request->name, $request->url);

        try {
            $this->commandBus->handle(new CreateSupplierCommand($supplier));
        } catch (\Exception $e) {
            $response->errors = [$e->getMessage()];
        }

        $presenter->present($response);
    }

}
