<?php

namespace App\Supply\Domain\UseCase\ListSupplier;

class ListSupplierResponse
{
    public iterable $suppliers;
}