<?php

namespace App\Supply\Domain\UseCase\ListSupplier;

interface ListSupplierPresenter
{
    public function present(ListSupplierResponse $response);
}