<?php

declare(strict_types=1);

namespace App\Supply\Domain\UseCase\ListSupplier;

use App\Core\Application\Query\QueryBus;
use App\Supply\Application\Query\ListSupplierQuery;

class ListSupplier
{

    private QueryBus $queryBus;

    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function execute(ListSupplierRequest $request, ListSupplierPresenter $presenter)
    {
        $response = new ListSupplierResponse();

        $response->suppliers = $this->queryBus->ask(new ListSupplierQuery());

        $presenter->present($response);
    }
}
