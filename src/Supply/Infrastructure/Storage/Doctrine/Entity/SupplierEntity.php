<?php

namespace App\Supply\Infrastructure\Storage\Doctrine\Entity;

use App\Supply\Domain\Model\Supplier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Supply\Infrastructure\Storage\Doctrine\Repository\DoctrineSupplierRepository")
 */
class SupplierEntity
{

    public int $id;

    public string $name;

    public string $url;

    /**
     * @param Supplier $supplier
     *
     * @return SupplierEntity
     */
    public static function fromSupplier(Supplier $supplier)
    {
        $entity = new self();
        $entity->name = $supplier->getName();
        $entity->url = $supplier->getUrl();
        return $entity;
    }

}
