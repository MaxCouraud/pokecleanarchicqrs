<?php


namespace App\Supply\Infrastructure\Storage\Doctrine\Repository;


use App\Supply\Domain\Model\Supplier;
use App\Supply\Domain\Repository\SupplierRepository;
use App\Supply\Infrastructure\Storage\Doctrine\Entity\SupplierEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DoctrineSupplierRepository extends ServiceEntityRepository implements SupplierRepository
{
    /**
     * DoctrineSupplierRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SupplierEntity::class);
    }

    public function getSuppliers(): array
    {
        $suppliers = $this->findAll();
        return array_map(
            function (SupplierEntity $supplier) {
                return new Supplier( $supplier->name, $supplier->url);
            },
            $suppliers
        );
    }

    public function save(Supplier $supplier): void
    {
        $this->getEntityManager()->persist(SupplierEntity::fromSupplier($supplier));
        $this->getEntityManager()->flush();
    }


}