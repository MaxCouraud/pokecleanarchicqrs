<?php

namespace App\Supply\Infrastructure\Storage\Memory\Repository;

use App\Supply\Domain\Model\Supplier;
use App\Supply\Domain\Repository\SupplierRepository;

class MemorySupplierRepository implements SupplierRepository
{
    /**
     * @return Supplier[]
     */
    public function getSuppliers(): array
    {
        return [
            new Supplier('Eurotechni', 'https://www.eurotechni.com'),
            new Supplier('Bois52', 'http://www.bois52-3apl.fr/'),
            new Supplier('Mercorne', 'https://www.mercorne.fr'),
            new Supplier('Brisa', 'https://www.brisa.fi')
        ];
    }

    public function save(Supplier $supplier): void
    {
        // TODO: Implement save() method.
    }
}