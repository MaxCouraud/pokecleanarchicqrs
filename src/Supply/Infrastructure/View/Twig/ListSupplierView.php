<?php


namespace App\Supply\Infrastructure\View\Twig;

use App\Supply\Ui\Http\Web\ViewModel\ListSupplierHtmlViewModel;
use App\Supply\Ui\Http\Web\ViewModel\ListSupplierJsonViewModel;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class ListSupplierView
{
    private $twig;

    public function __construct(Environment $twig,  RouterInterface $router)
    {
        $this->twig = $twig;
    }

    /**
     * @param ListSupplierHtmlViewModel $viewModel
     *
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function generateView(ListSupplierHtmlViewModel $viewModel): Response
    {
        return new Response($this->twig->render('supply/supplier/index.html.twig', ['viewModel' => $viewModel]), $viewModel->statusCode);
    }
}