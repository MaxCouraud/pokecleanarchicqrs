<?php


namespace App\Supply\Infrastructure\View\Twig;

use App\Supply\Ui\Http\Web\ViewModel\CreateSupplierHtmlViewModel;
use App\Supply\Ui\Http\Web\ViewModel\ListSupplierJsonViewModel;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class CreateSupplierView
{

    private $twig;

    public function __construct(Environment $twig, RouterInterface $router)
    {
        $this->twig = $twig;
    }

    /**
     * @param CreateSupplierHtmlViewModel $viewModel
     *
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function generateView(CreateSupplierHtmlViewModel $viewModel): Response
    {
        if ($viewModel->statusCode === 302) {
            return new RedirectResponse($viewModel->redirect, $viewModel->statusCode);
        }
        if ($viewModel->statusCode === 201) {
            return new Response($viewModel->supplier, $viewModel->statusCode);
        }
        if ($viewModel->statusCode === 500) {
            foreach ($viewModel->errors as $error){
                $viewModel->form->addError(new FormError($error));
            }
            return new Response($this->twig->render('supply/supplier/create.html.twig', ['form' => $viewModel->form->createView()]));
        }
    }

    public function generateFormView(FormInterface $form): Response
    {
        return new Response($this->twig->render('supply/supplier/create.html.twig', ['form' => $form->createView()]));
    }

}