<?php


namespace App\Supply\Infrastructure\View\Json;

use App\Supply\Ui\Http\Web\ViewModel\CreateSupplierHtmlViewModel;
use App\Supply\Ui\Http\Web\ViewModel\ListSupplierJsonViewModel;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class CreateSupplierView
{

    private $twig;

    public function __construct(Environment $twig, RouterInterface $router)
    {
        $this->twig = $twig;
    }

    /**
     * @param CreateSupplierHtmlViewModel $viewModel
     *
     * @return JsonResponse
     */
    public function generateView(CreateSupplierHtmlViewModel $viewModel): Response
    {
        if ($viewModel->statusCode === 201) {
            return new JsonResponse($viewModel->supplier, $viewModel->statusCode);
        }
    }
}