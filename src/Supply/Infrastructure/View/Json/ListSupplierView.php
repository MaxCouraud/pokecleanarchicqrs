<?php


namespace App\Supply\Infrastructure\View\Json;

use App\Supply\Ui\Http\Api\ViewModel\ListSupplierJsonViewModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class ListSupplierView
{

    /**
     * @param ListSupplierJsonViewModel $viewModel
     *
     * @return Response
     */
    public function generateView(ListSupplierJsonViewModel $viewModel): Response
    {
        return new JsonResponse($viewModel->suppliers);
    }

}