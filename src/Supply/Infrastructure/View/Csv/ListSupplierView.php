<?php


namespace App\Supply\Infrastructure\View\Csv;


use App\Supply\Domain\Model\Supplier;
use App\Supply\Ui\Cli\ViewModel\ListSupplierCliViewModel;
use App\Supply\Ui\Http\Web\ViewModel\ListSupplierHtmlViewModel;
use Symfony\Component\HttpFoundation\Response;

class ListSupplierView
{

    public function generateView(ListSupplierhtmlViewModel $viewModel): Response
    {
        $view = null;

        foreach ($viewModel->suppliers as $supplier) {
            $view .= $supplier->getName() . ',' . $supplier->getUrl() . "\n";
        }
        $response = new Response($view);
        $response->setStatusCode(200);
        $response->headers->add(['Content-type'=> 'application/csv']);
        return $response;
    }

}
