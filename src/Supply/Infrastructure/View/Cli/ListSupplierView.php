<?php


namespace App\Supply\Infrastructure\View\Cli;

use App\Supply\Domain\Model\Supplier;
use App\Supply\Ui\Cli\ViewModel\ListSupplierCliViewModel;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class ListSupplierView
{

    /**
     * @param ListSupplierCliViewModel $viewModel
     *
     * @return Response
     */
    public function generateView(ListSupplierCliViewModel $viewModel): string
    {
        $view = null;

        foreach ($viewModel->suppliers as $supplier) {
            $view .= "\e[0;91m" . $supplier->getName() . "\e[0m:\e[0;94m" . $supplier->getUrl() . "\e[0m\n";
        }

        return $view;
    }

}
