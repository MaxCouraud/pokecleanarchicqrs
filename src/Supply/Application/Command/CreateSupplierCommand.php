<?php


namespace App\Supply\Application\Command;


use App\Core\Application\Command\Command;
use App\Supply\Domain\Model\Supplier;

class CreateSupplierCommand implements Command
{
    public Supplier $supplier;

    /**
     * CreateSupplierCommand constructor.
     *
     * @param \App\Supply\Domain\Model\Supplier $supplier
     */
    public function __construct(Supplier $supplier)
    {
        $this->supplier = $supplier;
    }

}
