<?php


namespace App\Supply\Application\Command;

use App\Core\Application\Command\CommandHandler;
use App\Core\Application\Command\Command;
use App\Supply\Application\Query\ListSupplierQuery;
use App\Supply\Domain\Repository\SupplierRepository;

class CreateSupplierHandler implements CommandHandler
{

    private SupplierRepository $repository;

    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->repository = $supplierRepository;
    }

    /**
     * @param CreateSupplierCommand $command
     */
    public function __invoke(CreateSupplierCommand $command): void
    {
        $this->repository->save($command->supplier);
    }

}
