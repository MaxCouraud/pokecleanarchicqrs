<?php


namespace App\Supply\Application\Query;


use App\Core\Application\Query\QueryHandler;
use App\Supply\Domain\Model\Supplier;
use App\Supply\Domain\Repository\SupplierRepository;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplier;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierPresenter;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierRequest;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierResponse;
use App\Supply\Infrastructure\Storage\Memory\Repository\MemorySupplierRepository;

class ListSupplierHandler implements QueryHandler
{

    private SupplierRepository $repository;

    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->repository = $supplierRepository;
    }

    /**
     * @param ListSupplierQuery $query
     *
     * @return iterable
     */
    public function __invoke(ListSupplierQuery $query)
    {
        return $this->repository->getSuppliers();
    }

}
