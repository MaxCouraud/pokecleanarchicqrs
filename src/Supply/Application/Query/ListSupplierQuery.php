<?php


namespace App\Supply\Application\Query;


use App\Core\Application\Query\Query;
use App\Supply\Domain\Model\Supplier;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierPresenter;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierRequest;
use App\Supply\Domain\UseCase\ListSupplier\ListSupplierResponse;

class ListSupplierQuery implements Query
{


}
