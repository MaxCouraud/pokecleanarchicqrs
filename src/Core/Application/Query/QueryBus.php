<?php

declare(strict_types=1);

namespace App\Core\Application\Query;

interface QueryBus
{
    public function ask(Query $query);
}
