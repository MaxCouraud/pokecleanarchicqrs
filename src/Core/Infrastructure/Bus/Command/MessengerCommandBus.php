<?php


namespace App\Core\Infrastructure\Bus\Command;


use App\Core\Application\Command\CommandBus;
use App\Core\Application\Command\Command;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class MessengerCommandBus implements CommandBus
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function handle(Command $command)
    {
        try {
            $envelope = $this->messageBus->dispatch($command);

            $stamp = $envelope->last(HandledStamp::class);

            return $stamp->getResult();
        } catch (HandlerFailedException $e) {
            throw $e;
        }
    }
}
